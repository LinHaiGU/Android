## 在Binder中使用代理

Binder是Android中的一种跨进程通信方式，是客户端和服务端通信的媒介，当客户端绑定服务时，服务端会返回一个包含服务端业务调用的Binder对象，
通过这个Binder对象，客户端就可以获取服务端提供的服务或数据。

通常我们创建AIDL文件时会自动生成Java文件，格式如下：

```
public interface XXXXX extends android.os.IInterface{
    省略代码...
    private static class Proxy implements XXXX {
        Proxy(IBinder remote) {
            mRemote = remote;
        }
    }
    省略代码...
}
```

可以很清楚的看到内部类Proxy接收一个IBinder参数，这个参数实际上就是服务端Service中的onBind方法返回的Binder对象在客户端重新打包后的结果，因为客户端无法直接通过这个打包的Binder和服务端通信，因此客户端必须借助Proxy类来和服务端通信，这里Proxy的作用就是代理的作用，客户端所有的请求全部通过Proxy来代理，具体工作流程为：Proxy接收到客户端的请求后，会将客户端的请求参数打包到Parcel对象中，然后将Parcel对象通过它内部持有的Ibinder对象传送到服务端，服务端接收数据、执行方法后返回结果给客户端的Proxy，Proxy解析数据后返回给客户端的真正调用者。

> 更新中......

