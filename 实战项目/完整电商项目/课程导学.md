## 教学特点

> 一边设计项目框架，一边实现完整电商APP。

> 让大家掌握项目架构设计思想与技巧，并运用到实际的项目中。

> 既提高项目架构能力，又提高项目实战能力内功、外功双重提高。

---

## 为什么这么教

> 授之以渔，并且授之以渔。

> 单独讲解电商业务实现，对熟练功能的同学没有本质提高。

> 单独讲解框架，太过空洞，不接地气。

> 在电商完整的实战中，提炼通用框架，打通任督二脉，走好通往架构师的第一站。

> 在电商完整的实战中验证框架，架构业务两不误。

---

## 框架介绍

> 从零开始，每一行代码和设计思想360度无死角讲解。

> 每设计和实现一个框架功能，在相应的电商实战中予以验证与实践。

> 以让初学者都能使用为宗旨开发极简框架。

> 融合设计模式，创造出适合自己的模式。

> 高性能傻瓜式网络框架。

> 高复用性UI框架，极简操作，无惧业务逻辑的多变。

> 高性能二维码，图片处理等多媒体框架。

> 完善的Web混合应用解决方案。

> 全部使用最新最酷的技术，拒绝老旧。

---

## 项目结构

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180228143627.png)

首页有一个核心module，在核心module下面有若干个功能module，在功能module之上就是项目module，也就是类型为application的module，这个项目module包含我们项目的一些基础元信息，比如说包名、application id以及一些特有的信息，比如说一些第三方平台需要的appId、AppSercet，都会写在这个
具体的module里。

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180228145608.png)

注解module包含各种annotation，这里面是记录的元信息，配合代码生成器module，也就是通过annotationProcessor或apt调用的module，来实现动态生成代码的功能。

最终项目结构：

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180228150236.png)

注意事项：

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180228150658.png)

## 效果图

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180228151207.png)

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180228151450.png)

等等，具体效果图，后续给出。
