
## 总结

Activity的attach方法中创建PhoneWindow时，PhoneWindow持有Activity的引用，DecorView是PhoneWindow的内部类，事件是由DecorView的dispatchTouchEvent方法传递给Activity的（底层硬件接受事件
传递给WindowManagerService,由它进行跨进程通知ViewRootImpl执行dispatchInputEvent方法，最终交由DecorView的dispatchTouchEvent方法传递给Activity）

## 分析

点击事件流程大致如下
> Activity->PhoneWindow->DecorView

> DecorView再遍历传递

下面讨论Activity的事件是从哪里来的？

---

Activity实现了一个接口：Window.Callback。

```
public class Activity extends ContextThemeWrapper
        implements LayoutInflater.Factory2,
        Window.Callback, KeyEvent.Callback,
        OnCreateContextMenuListener, ComponentCallbacks2,
        Window.OnWindowDismissedCallback
```

Window.Callback代码如下：

```
public interface Callback {
        
        public boolean dispatchKeyEvent(KeyEvent event);

       
        public boolean dispatchTouchEvent(MotionEvent event);

        /**
       
        public boolean dispatchTrackballEvent(MotionEvent event);
        ...（省略若干代码，下同）
```


Window.Callback和点击事件有关，如果外界想要传递事件给Activity，那么它必须持有Activity的引用，在Activity的attach方法中，有
这么一段：

```
//PhoneWindow是Window的唯一实现类
//DecorView是由PhoneWindow负责添加
final void attach(Context context, ActivityThread aThread,
            Instrumentation instr, IBinder token, int ident,
            Application application, Intent intent, ActivityInfo info,
            CharSequence title, Activity parent, String id,
            NonConfigurationInstances lastNonConfigurationInstances,
            Configuration config, String referrer, IVoiceInteractor voiceInteractor) {
        attachBaseContext(context);

        mFragments.attachHost(null /*parent*/);

        mWindow = new PhoneWindow(this);
        mWindow.setCallback(this);
        mWindow.setOnWindowDismissedCallback(this);
        mWindow.getLayoutInflater().setPrivateFactory(this);
```

DecorView是PhoneWindow里面的一个内部类：

上面代码中mWindow.setCallback(this)Activity当前引用传递给了PhoneWindow，也就是PhoneWindow持有了Activity引用。

Activity启动后，在onResume以后，DecorView在开始添加到WindowManager并且显示出来：

```
    void makeVisible() {
        if (!mWindowAdded) {
            ViewManager wm = getWindowManager();
            wm.addView(mDecor, getWindow().getAttributes());
            mWindowAdded = true;
        }
        mDecor.setVisibility(View.VISIBLE);
    }
```

> WindowManager是一个接口，实现该接口的是WindowManagerImpl，WindowManagerImpl里面有一个成员变量WindowManagerGlobal，而真正的实现则是在WindowManagerGlobal了，类似代理，
只不过WindowManagerGlobal是个没有实现WindowManager的类

归根究底，最终调用了WindowManagerGlobal的addView方法。

```
    public void addView(View view, ViewGroup.LayoutParams params,
            Display display, Window parentWindow) {
        ViewRootImpl root;
        View panelParentView = null;
        ...这里省略了一堆代码
        root = new ViewRootImpl(view.getContext(), display);
        view.setLayoutParams(wparams);
        mViews.add(view);
        mRoots.add(root);
        mParams.add(wparams);

        // do this last because it fires off messages to start doing things
        try {
            //这里将DecorView传递给了ViewRootImpl
            root.setView(view, wparams, panelParentView);
        } catch (RuntimeException e) {
            // BadTokenException or InvalidDisplayException, clean up.
            synchronized (mLock) {
                final int index = findViewLocked(view, false);
                if (index >= 0) {
                    removeViewLocked(index, true);
                }
            }
            throw e;
        }
    }
```

上面代码在创建ViewRootImpl时，通过setView方法将DecorView的引用传递了过去，这时ViewRootImpl持有了DecorView。

在ViewRootImpl的setView方法（此方法运行在UI线程）中，会通过跨进程的方式向WMS（WindowManagerService）发起一个调用，从而将DecorView最终添加到Window上，在这个过程中，
ViewRootImpl、DecorView和WMS会彼此向关联，同时会创建InputChannel、InputQueue和WindowInputEventReceiver来接受点击事件的消息。

用户触摸行为由硬件来捕获，然后交由WindowManagerService来处理。

在ViewRootImpl中，有一个方法，叫做dispatchInputEvent，如下：

```
    public void dispatchInputEvent(InputEvent event, InputEventReceiver receiver) {
        SomeArgs args = SomeArgs.obtain();
        args.arg1 = event;
        args.arg2 = receiver;
        Message msg = mHandler.obtainMessage(MSG_DISPATCH_INPUT_EVENT, args);
        msg.setAsynchronous(true);
        mHandler.sendMessage(msg);
    }
```

InputEvent有2个子类：KeyEvent和MotionEvent，其中KeyEvent表示键盘事件，而MotionEvent表示点击事件。在上面的代码中，mHandler是一个在UI线程创建的Handder，
所以它会把执行逻辑切换到UI线程中。

```
 final ViewRootHandler mHandler = new ViewRootHandler();
 ...
 case MSG_DISPATCH_INPUT_EVENT: {
                SomeArgs args = (SomeArgs)msg.obj;
                InputEvent event = (InputEvent)args.arg1;
                InputEventReceiver receiver = (InputEventReceiver)args.arg2;
                enqueueInputEvent(event, receiver, 0, true);
                args.recycle();
            }
```

最终调用deliverInputEvent方法来处理点击事件的消息，如下：

```
    private void deliverInputEvent(QueuedInputEvent q) {
        Trace.asyncTraceBegin(Trace.TRACE_TAG_VIEW, "deliverInputEvent",
                q.mEvent.getSequenceNumber());
        if (mInputEventConsistencyVerifier != null) {
            mInputEventConsistencyVerifier.onInputEvent(q.mEvent, 0);
        }

        InputStage stage;
        if (q.shouldSendToSynthesizer()) {
            stage = mSyntheticInputStage;
        } else {
            stage = q.shouldSkipIme() ? mFirstPostImeInputStage : mFirstInputStage;
        }

        if (stage != null) {
            stage.deliver(q);
        } else {
            finishInputEvent(q);
        }
    }
```

在ViewRootImpl中，有一系列类似于InputStage（输入事件舞台）的概念，每种InputStage可以处理一定的事件类型，
比如AsyncInputStage、ViewPreImeInputStage、ViewPostImeInputStage等。当一个InputEvent到来时，ViewRootImpl会寻找合适它的InputStage来处理。
对于点击事件来说，ViewPostImeInputStage可以处理它，ViewPostImeInputStage中，有一个processPointerEvent方法，如下，它会调用mView的dispatchPointerEvent方法，
注意，这里的mView其实就是DecorView。

```
        private int processPointerEvent(QueuedInputEvent q) {
            final MotionEvent event = (MotionEvent)q.mEvent;

            mAttachInfo.mUnbufferedDispatchRequested = false;
            boolean handled = mView.dispatchPointerEvent(event);
            if (mAttachInfo.mUnbufferedDispatchRequested && !mUnbufferedInputDispatch) {
                mUnbufferedInputDispatch = true;
                if (mConsumeBatchedInputScheduled) {
                    scheduleConsumeBatchedInputImmediately();
                }
            }
            return handled ? FINISH_HANDLED : FORWARD;
        }
```

在View的实现中，dispatchPointerEvent的逻辑如下，这样一来，点击事件就传递给了DecorView的dispatchTouchEvent方法。


```
    public final boolean dispatchPointerEvent(MotionEvent event) {
        if (event.isTouchEvent()) {
            return dispatchTouchEvent(event);
        } else {
            return dispatchGenericMotionEvent(event);
        }
    }
```

DecorView的dispatchTouchEvent的实现如下，需要强调的是，DecorView是PhoneWindow的内部类，还记得前面提到的Window.Callback吗？
没错，在下面的代码中，这个cb对象其实就是Activity，就这样点击事件就传递给了Activity了。


```
        public boolean dispatchTouchEvent(MotionEvent ev) {
            final Callback cb = getCallback();
            return cb != null && !isDestroyed() && mFeatureId < 0 ? cb.dispatchTouchEvent(ev)
                    : super.dispatchTouchEvent(ev);
        }
```





