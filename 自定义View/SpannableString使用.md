#SpannableString用途

SpannableString是一种字符串类型，可以设置文本给TextView，也可以通过setSpan方法实现字符串各种形式风格的显示。
```
    setSpan(Object what,int start,int end,int flags)
    what:设置的格式
    start:起始下标
    end:结束下标
    flags:是否包含下标，有四种属性
          Spanned.SPAN_INCLUSIVE_EXCLUSIVE:从起始下标到结束下标，包括起始下标
          Spanned.SPAN_INCLUSIVE_INCLUSIVE:同时包括起始下标和结束下标
          Spanned.SPAN_EXCLUSIVE_EXCLUSIVE:都不包括起始和结束下标
          Spanned.SPAN_EXCLUSIVE_INCLUSIVE:包括结束下标。
```

#Span常用的格式

ForegroundColorSpan:为文本设置前景色，效果和TextView的setTextColor()类似，实现方式如下：

![ForegroundColorSpan](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180224095206.png)
```
        SpannableString spannableString = new SpannableString("设置文字的前景色为淡蓝色");
        ForegroundColorSpan colorSpan=new ForegroundColorSpan(Color.parseColor("#0099EE"));
        spannableString.setSpan(colorSpan,9,spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView.setText(spannableString);
```

BackgroundColorSpan:为文本设置背景色，效果和TextView的setBackground()类似，实现方式如下：

![BackgroundColorSpan](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180224095413.png)
```
        SpannableString spannableString = new SpannableString("设置文字的背景色为淡绿色");
        BackgroundColorSpan colorSpan=new BackgroundColorSpan(Color.parseColor("#AC00FF30"));
        spannableString.setSpan(colorSpan,9,spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView.setText(spannableString);
```

RelativeSizeSpan:设置文字相对大小，在TextView原有的文字大小的基础上，相对文字大小，实现方式如下：

![RelativeSizeSpan](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180224095525.png)
```
        SpannableString spannableString = new SpannableString("万丈高楼平地起");

        RelativeSizeSpan sizeSpan01=new RelativeSizeSpan(1.2f);
        RelativeSizeSpan sizeSpan02=new RelativeSizeSpan(1.4f);
        RelativeSizeSpan sizeSpan03=new RelativeSizeSpan(1.6f);
        RelativeSizeSpan sizeSpan04=new RelativeSizeSpan(1.8f);
        RelativeSizeSpan sizeSpan05=new RelativeSizeSpan(1.6f);
        RelativeSizeSpan sizeSpan06=new RelativeSizeSpan(1.4f);
        RelativeSizeSpan sizeSpan07=new RelativeSizeSpan(1.2f);
        
        spannableString.setSpan(sizeSpan01,0,1,Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(sizeSpan02,1,2,Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(sizeSpan03,2,3,Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(sizeSpan04,3,4,Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(sizeSpan05,4,5,Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(sizeSpan06,5,6,Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(sizeSpan07,6,7,Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        
        textView.setText(spannableString);
```

StrikethroughSpan:为文本设置中划线，实现方式如下：

![StrikethroughSpan](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180224095628.png)
```
        SpannableString spannableString = new SpannableString("为文字设置删除线");
        StrikethroughSpan strikethroughSpan=new StrikethroughSpan();
        spannableString.setSpan(strikethroughSpan,5,spannableString.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        
        textView.setText(spannableString);
```

UnderlineSpan:为文本设置下划线，实现方式如下：

![UnderlineSpan](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180224095722.png)
```
        SpannableString spannableString = new SpannableString("为文字设置下划线");
        UnderlineSpan underlineSpan=new UnderlineSpan();
        spannableString.setSpan(underlineSpan,5,spannableString.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        textView.setText(spannableString);
```


SuperscriptSpan:设置上标，实现方式如下：

![SuperscriptSpan](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180224095821.png)
```
        SpannableString spannableString = new SpannableString("为文字设置上标");
        SuperscriptSpan superscriptSpan=new SuperscriptSpan();
        spannableString.setSpan(superscriptSpan,5,spannableString.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        textView.setText(spannableString);
```

SubscriptSpan:设置下标，实现方式如下：

![SubscriptSpan](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180224095831.png)
```
        SpannableString spannableString = new SpannableString("为文字设置下标");
        SuperscriptSpan superscriptSpan=new SuperscriptSpan();
        spannableString.setSpan(superscriptSpan,5,spannableString.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        textView.setText(spannableString);
```

StyleSpan:为文字设置风格，和TextView属性textStyle类似，实现方式如下：

![StyleSpan](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180224100012.png)
```
        SpannableString spannableString = new SpannableString("为文字设置粗体、斜体风格");
        StyleSpan styleSpan_B = new StyleSpan(Typeface.BOLD);
        StyleSpan styleSpan_I = new StyleSpan(Typeface.ITALIC);
        spannableString.setSpan(styleSpan_B, 5, 7, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(styleSpan_I, 8, 10, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView.setText(spannableString);
```

ImageSpan:设置文本图片，实现方式如下：

![ImageSpan](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180224100021.png)
```
        SpannableString spannableString = new SpannableString("在文本中添加表情（表情）");
        Drawable drawable=getResources().getDrawable(R.mipmap.ic_launcher);
        drawable.setBounds(0,0,42,42);
        ImageSpan imageSpan=new ImageSpan(drawable);
        spannableString.setSpan(imageSpan, 6, 8, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView.setText(spannableString);
```

ClickableSpan:设置可点击的文本，设置这个属性的文本可以响应用户点击事件，实现方式如下：

![ClickableSpan](https://gitlab.com/LinHaiGU/Android/raw/master/picture/0%20(1).gif)
```
    private void init() {
        SpannableString spannableString = new SpannableString("为文字设置点击事件");
        MyClickableSpan clickableSpan=new MyClickableSpan("http://www.baidu.com");
        spannableString.setSpan(clickableSpan,5,spannableString.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.parseColor("#36969696"));
        textView.setText(spannableString);
    }

    class MyClickableSpan extends ClickableSpan {

        private String content;

        public MyClickableSpan(String content) {
            this.content = content;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setUnderlineText(false);
        }

        @Override
        public void onClick(View widget) {
            Intent intent = new Intent(AmountMoneyConfirmationAct.this, LoginAct.class);
            Bundle bundle = new Bundle();
            bundle.putString("content", content);
            intent.putExtra("bundle", bundle);
            startActivity(intent);
        }
    }
```
ds.setUnderlineText()控制是否可点击文本显示下划线。

        使用ClickableSpan的文本如果想真正实现点击作用，必须为TextView设置setMovementMethod方法，否则点击没有响应，setHighlightColor方法控制点击的背景色。

URLSpan:设置超链接文本，实现方式如下：

![URLSpan](https://gitlab.com/LinHaiGU/Android/raw/master/picture/0.gif)
```
        SpannableString spannableString = new SpannableString("为文字设置超链接");
        URLSpan urlSpan=new URLSpan("http://www.baidu.com");
        spannableString.setSpan(urlSpan,5,spannableString.length(),Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setHighlightColor(Color.parseColor("#36969696"));
        textView.setText(spannableString);
```

#SpannableStringBuilder

StringBuilder可以使用append()方法实现字符串拼接，非常方便。同样，SpannableString中也有SpannableStringBuilder，顾名思义，就是实现对，SpannableString的一个拼接效果，同样是append()方法，可以实现各种风格效果的SpannableString拼接








