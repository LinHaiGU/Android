## Fragment为什么被称为第五大组件

### Fragment为什么被称为第五大组件

Fragment使用广泛，有自己的生命周期（必须依附Activity），动态灵活的加载到Activity中去，Android 3.0 引入，常用模式有Fragment+ViewPager。

### Fragment加载到Activity的两种方式

+ 添加Fragment到Activity的布局文件当中。
+ 在Activity的代码中动态添加Fragment。

> 第一种方式虽然简单但灵活性不够。添加Fragment到Activity的布局文件当中,就等同于将Fragment及其视图与activity的视图绑定在一起，且在activity的生命周期过程中，无法切换fragment视图。
<br>第二种方式比较复杂，但也是唯一一种可以在运行时控制fragment的方式（加载、移除、替换）。

第二种方式代码如下：<br>
```
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@color/background">

    <FrameLayout
        android:id="@+id/id_fl_homepage"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_above="@id/id_bottom_menu" />
</RelativeLayout>
```

```
    private static Fragment mContentFragment;

    /**
     * Fragment的切换
     *
     * @param fragment Fragment
     */
    private void switchFragment(Fragment fragment) {
        if (mContentFragment != fragment) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.hide(mContentFragment);
            if (fragment.isAdded()) {
                fragmentTransaction.show(fragment);
            } else {
                fragmentTransaction.add(R.id.id_fl_homepage, fragment);
            }
            fragmentTransaction.commitAllowingStateLoss();
            mContentFragment = fragment;
        }
    }
```

### FragmentPagerAdapter与FragmentStatePagerAdatper区别

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180227112936.png)

FragmentPagerAdapter适用于页面较少的情况下，FragmentStatePagerAdapter适用于页面较多的情况下。

查看FragmentStatePagerAdapter源码：

```
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        Fragment fragment = (Fragment) object;

        if (mCurTransaction == null) {
            mCurTransaction = mFragmentManager.beginTransaction();
        }
        if (DEBUG) Log.v(TAG, "Removing item #" + position + ": f=" + object
                + " v=" + ((Fragment)object).getView());
        while (mSavedState.size() <= position) {
            mSavedState.add(null);
        }
        mSavedState.set(position, fragment.isAdded()
                ? mFragmentManager.saveFragmentInstanceState(fragment) : null);
        mFragments.set(position, null);
        //真正的释放了Fragment内存。
        mCurTransaction.remove(fragment);
    }
```

> 重点在最后一行，mCurTransaction.remove(fragment);表示释放了Fragment内存。

再查看FragmentPageAdapter源码：

```
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (mCurTransaction == null) {
            mCurTransaction = mFragmentManager.beginTransaction();
        }
        if (DEBUG) Log.v(TAG, "Detaching item #" + getItemId(position) + ": f=" + object
                + " v=" + ((Fragment)object).getView());
        mCurTransaction.detach((Fragment)object);
    }
```

> 同样看最后一行调用了FragmentTransaction的detach方法，detach方法只是将Fragment UI和Activity UI脱离开，并不会回收内存。

## Fragment生命周期

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/201329240812252.png)

Fragment不是独立，它依附于Activity，

Fragment与Activity生命周期对比图：

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/201329372531026.png)

## Fragment之间的通信

+ 在Fragment中调用Activity中的方法: getActivity。
+ 在Activity中调用Fragment中的方法: 接口回调。
+ 在Fragment中调用Fragment中的方法: findFragmentById

## Fragment管理器:FragmentManager

FragmentManager方法：replace、add、remove。

replace作用是替换Fragment实例，add作用是添加Fragment实例,remove移除Fragment。
