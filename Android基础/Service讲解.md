## Service是什么？

Service是Android四大组件之一，Service（服务）是一个一种可以在后台执行长时间运行操作而没有用户界面的应用组件。

> 运行在主线程中，不能做长时间的耗时操作。

---

## Service的应用场景，以及和Thread区别

+ 定义：Thread:程序执行的最小单元-线程，可以执行异步操作。 <br>
        Service:如果是本地Service，运行在主线程中，Thread相对独立，而Service依附于它所在的主线程。
+ 实际开发：在Android中，线程一般指的是工作线程，也就是后台线程，做一些耗时操作的线程，而主线程负责一些UI的绘制，主线程不能做耗时操作；Service也是运行在主线程中，不能做耗时操作，如果一定要在Service中做耗时操作，可以开启单独的线程。
+ 应用场景：当需要阻塞UI线程做一些操作时，应该开启子线程；而Service经常需要长时间在后台运行，不需要销毁的情况下，可以使用Service。

---

## 开启Service的两种方式以及区别

+ startService

```
public class StartService extends Service {

    /**
     * 绑定服务时才会调用
     * 必须要实现的方法
     *
     * @param intent
     * @return
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * 首次创建服务时，系统将调用此方法来执行一次（在调用 onStartCommand()或onBind()之前）
     * 如果服务已经在运行，则不会调用此方法，该方法只调用一次
     */
    @Override
    public void onCreate() {
        System.out.println("onCreate invoke");
        Log.d("startservice", "StartService thread id is" + Thread.currentThread().getId());
        super.onCreate();
    }

    /**
     * 每次通过startService()方法启动Service时都会回调
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("onStartCommand invoke");
        Log.d("startservice", "StartService thread id is" + Thread.currentThread().getId());
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * 服务销毁时回调
     */
    @Override
    public void onDestroy() {
        System.out.println("onDestroy invoke");
        super.onDestroy();
    }
}

```

> onStartCommand方法返回的是START_STICKY，表明当Service因为内存不足而被杀掉后，一段时间内存再次空闲时，系统会尝试重新创建这个Service，但方法中的Intent为空。

当开启服务，服务会在后台一直运行，这时如果将Activity销毁，对服务不受任何影响（除非手动关闭Service）。

> 定义一个类继承Service<br>
> 在Manifest.xml文件中配置该Service<br>
> 使用Context的startService(Intent)方法启动该Service<br>
> 不再使用时，调用stopService(Intent)方法停止该服务

+ bindService

```
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;


public class BindService extends Service {

    private final static String TAG = "BIND_SERVICE";
    private int count;
    private boolean quit;
    private Thread thread;
    private LocalBinder binder = new LocalBinder();

    /**
     * 创建Binder对象，返回给客户端和Activity使用，提供数据交换的接口
     */
    public class LocalBinder extends Binder {
        //声明一个方法，提供给客户端调用
        BindService getService() {
            //返回当前对象,在客户端可以调用Service的公共方法
            return BindService.this;
        }
    }

    /**
     * 把Binder类返回给客户端
     *
     * @param intent
     * @return
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!quit) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    count++;
                }
            }
        });
        thread.start();
    }

    /**
     * 解除绑定时调用
     *
     * @param intent
     * @return
     */
    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        quit = true;
        super.onDestroy();
    }

    /**
     * 公共方法
     *
     * @return
     */
    public int getCount() {
        return count;
    }
}

```

```
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MAIN_ACTIVITY";

    private Button btn_get;
    private Button btn_bind;
    private Button btn_unbind;

    private ServiceConnection mServiceConnection;
    private BindService mBindService;
    private Intent mIntentService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initService();
        initEvent();
    }

    private void initView() {
        btn_get = (Button) findViewById(R.id.btn_get);
        btn_bind = (Button) findViewById(R.id.btn_bind);
        btn_unbind = (Button) findViewById(R.id.btn_unbind);
    }

    private void initEvent() {

        btn_bind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //绑定服务
                bindService(mIntentService, mServiceConnection, Service.BIND_AUTO_CREATE);
            }
        });

        btn_unbind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //解除绑定的服务
                if (mBindService != null) {
                    mBindService = null;
                    unbindService(mServiceConnection);
                }
            }
        });

        btn_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBindService != null) {
                    //通过绑定服务传递的Binder对象，获取Service暴露处理的数据
                    Log.d(TAG, "从服务器获取数据：" + mBindService.getCount());
                } else {
                    Log.d(TAG, "还没绑定服务");
                }
            }
        });

    }

    private void initService() {
        mIntentService = new Intent(this, BindService.class);

        mServiceConnection = new ServiceConnection() {
            /**
             * 与服务器交互的接口方法，绑定服务的时候被回调，在这个方法中获取绑定Service传递过来的IBinder对象。
             * 通过这个IBinder对象，实现宿主和Service交互。
             */
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                BindService.LocalBinder binder = (BindService.LocalBinder) service;
                mBindService = binder.getService();
            }

            /**
             * 当取消绑定的时候被回调，但正常情况下是不被调用的，它的调用时机是当Service服务被意外销毁时，
             * 例如内存的资源不足时这个方法才被自动调用。
             */
            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBindService = null;
            }
        };
    }
}

```

> 输出：02-27 16:10:04.323 22336-22336/com.gg01.gg01 D/MAIN_ACTIVITY: 还没绑定服务<br>
02-27 16:10:09.479 22336-22336/com.gg01.gg01 D/MAIN_ACTIVITY: 从服务器获取数据：2<br>
02-27 16:10:11.012 22336-22336/com.gg01.gg01 D/MAIN_ACTIVITY: 从服务器获取数据：4<br>
02-27 16:10:15.652 22336-22336/com.gg01.gg01 D/MAIN_ACTIVITY: 从服务器获取数据：8<br>
02-27 16:10:17.451 22336-22336/com.gg01.gg01 D/MAIN_ACTIVITY: 从服务器获取数据：10<br>
02-27 16:10:20.429 22336-22336/com.gg01.gg01 D/MAIN_ACTIVITY: 还没绑定服务<br>
02-27 16:10:23.721 22336-22336/com.gg01.gg01 D/MAIN_ACTIVITY: 从服务器获取数据：1<br>
02-27 16:10:24.784 22336-22336/com.gg01.gg01 D/MAIN_ACTIVITY: 从服务器获取数据：2<br>

绑定服务时，服务与Activity处于绑定状态，允许Activity与Service进行数据交互、发送请求、获取结果等等，如果Service和Activity不在同一个进程中，可以通过进程间通信来传输数据。

> 1、创建BindService服务端，继承自Service，并在类中创建一个实现IBinder接口的实例对象并提供公共方法给客户端调用。<br>
> 2、从onBind()回调方法返回此Binder实例。<br>
> 3、在客户端中，从onServiceConnected()回调方法接收Binder，并使用提供的方法调用绑定服务。




