## 任务栈

Android是以栈的形式来管理Activity的（后进先出）。

---

## Activity启动模式

+ standard:每次启动Activity都会重新创建Activity实例并放到栈顶。
+ singletop:栈顶复用模式，如果创建的Activity已经处于栈顶，会复用这个Activity，同时它的onNewIntent方法会被回调；反之会重新创建Activity并放到栈顶。
+ singletask:栈内复用模式，检测整个任务栈中是否存在该Activity，存在会将该Activity以上的所有Activity全部出栈，并回调该Activity的onNewIntent方法。
+ singleInstance:单实例模式。具有singleTask模式的所有特性，并且具有此种模式的Activity只能单独地位于一个任务栈中。

---

## scheme跳转协议

android中的scheme是一种页面内跳转协议，是一种非常好的实现机制，通过定义自己的scheme协议，可以非常方便的跳转APP中的各个页面；通过scheme协议，服务器可以定制化告诉APP跳转哪个页面，可以通过通知栏消息定制化跳转页面，可以通过H5页面跳转页面等。
