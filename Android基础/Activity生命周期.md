## Activity的4种状态

+ running:Activity处于活动状态,用户点击屏幕，屏幕做出响应，Activity处于栈顶。

+ paused:Activity失去焦点或者被非全屏的Activity占据（或是透明的Activity放置在栈顶），Activity只是失去和用户的交互，内存状态信息、成员变量都存在（内存不紧张）。

+ stopped:当前Activity被另一个Activity完全被覆盖，这时不可见，内存状态信息、成员变量都存在（内存不紧张）。

+ killed:Activity被系统回收。

---

## Activity的生命周期

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/timg.jpg)

> Activity启动->onCreate()->onStart()->onResume()<br>
> onCreate:Activity被创建时回调，可以做些初始化操作（setContentView、数据加载）。<br>
> onStart:Activity正在启动，Activity处于用户可见的状态，但并不属于前台显示，这时用户不能交互。<br>
> onResume:Activity已经在前台可见，与用户交互。

点击Home键回到主界面(Activity不可见)->onPause()->onStop()

> onPause:整个Activity处于停止状态（可见但不能被触摸）。<br>
> onStop:整个Activity已经被停止（覆盖）。

当我们再次回到原Activity时->onRestart()->onStart()->onResume()

> onRestart:Activity正在被启动（处于不可见状态到可见状态）。<br>
onStart:Activity正在启动，Activity处于用户可见的状态，但并不属于前台显示，这时用户不能交互。<br>
onResume:Activity已经在前台可见，与用户交互。

退出当前Activity时->onPause()->onStop()->onDestroy()

> onDestroy:Activity正在被销毁，可以做回收工作。

---

## Android进程优先级

前台/可见/服务/后台/空

+ 前台：处于与用户交互的Activity，与前台服务绑定的Activity。
+ 可见：用户不能交互。
+ 服务：后台开启Service服务。
+ 后台：前台进程切换到后台。
+ 空：不属于前面4种的进程属于空进程。






