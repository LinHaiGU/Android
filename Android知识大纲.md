# Android基础
[Activity生命周期](https://gitlab.com/LinHaiGU/Android/blob/master/Android%E5%9F%BA%E7%A1%80/Activity%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F.md)

[Android中的启动模式](https://gitlab.com/LinHaiGU/Android/blob/master/Android%E5%9F%BA%E7%A1%80/Android%E4%B8%AD%E7%9A%84%E5%90%AF%E5%8A%A8%E6%A8%A1%E5%BC%8F.md)

[Fragment讲解](https://gitlab.com/LinHaiGU/Android/blob/master/Android%E5%9F%BA%E7%A1%80/Fragment%E8%AE%B2%E8%A7%A3.md)

[Service讲解](https://gitlab.com/LinHaiGU/Android/blob/master/Android%E5%9F%BA%E7%A1%80/Service%E8%AE%B2%E8%A7%A3.md)

---

# 自定义View

### 特效实现原理
[SpannableString使用](https://gitlab.com/LinHaiGU/Android/blob/master/%E8%87%AA%E5%AE%9A%E4%B9%89View/SpannableString%E4%BD%BF%E7%94%A8.md)  

[水波纹效果](https://gitlab.com/LinHaiGU/Android/blob/master/%E8%87%AA%E5%AE%9A%E4%B9%89View/%E6%B0%B4%E6%B3%A2%E7%BA%B9%E6%95%88%E6%9E%9C.md)

### 相关知识
[事件是如何传递给Activity的](https://gitlab.com/LinHaiGU/Android/blob/master/%E8%87%AA%E5%AE%9A%E4%B9%89View/%E4%BA%8B%E4%BB%B6%E6%98%AF%E5%A6%82%E4%BD%95%E4%BC%A0%E9%80%92%E7%BB%99Activity%E7%9A%84.md)

---

# 性能优化

[内存泄露总结](https://gitlab.com/LinHaiGU/Android/blob/master/%E6%80%A7%E8%83%BD%E4%BC%98%E5%8C%96/%E5%86%85%E5%AD%98%E6%B3%84%E9%9C%B2%E6%80%BB%E7%BB%93.md)

[流畅度测试](https://gitlab.com/LinHaiGU/Android/blob/master/%E6%80%A7%E8%83%BD%E4%BC%98%E5%8C%96/%E6%B5%81%E7%95%85%E5%BA%A6%E6%B5%8B%E8%AF%95.md)

---

# 多线程

### 基础知识
[了解ThreadLocal](https://gitlab.com/LinHaiGU/Android/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B/%E4%BA%86%E8%A7%A3ThreadLocal.md)

[Java内存模型](https://gitlab.com/LinHaiGU/Android/blob/master/%E5%A4%9A%E7%BA%BF%E7%A8%8B/Java%E5%86%85%E5%AD%98%E6%A8%A1%E5%9E%8B.md)

---

# 设计模式
[代理模式](https://gitlab.com/LinHaiGU/Android/blob/master/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F/%E4%BB%A3%E7%90%86%E6%A8%A1%E5%BC%8F.md)

### Android中的设计模式
[Android中的代理模式](https://gitlab.com/LinHaiGU/Android/blob/master/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F/Android%E4%B8%AD%E7%9A%84%E4%BB%A3%E7%90%86%E6%A8%A1%E5%BC%8F.md)

---

# 网络
[HTTP协议](https://gitlab.com/LinHaiGU/Android/blob/master/%E7%BD%91%E7%BB%9C/HTTP%E5%8D%8F%E8%AE%AE.md)