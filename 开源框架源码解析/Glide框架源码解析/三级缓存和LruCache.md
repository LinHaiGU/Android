## 内存-本地-网络

## 内存缓存是如何实现的？

## LRU(Least Recently Used)缓存算法

+ LruCache中将LinkedHashMap的顺序设置为LRU顺序来实现LRU缓存。
+ 每次调用get则将该对象移动链表的尾端。调用put插入新的对象也是存储在链表尾端。
+ 当内存缓存达到设定的最大值时，将链表头部的对象（近期最少用到的）移除。


