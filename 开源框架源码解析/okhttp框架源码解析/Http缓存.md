## 强制缓存

+ 1、Expires

Expires的值为服务器端返回的到期时间。

问题：到期时间是由服务端生成的。

---

+ 2、Cache-Control

Cache-control是由服务器返回的Response中添加的头信息

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20180302145505.png)

private：客户端可以缓存。

public：客户端和代理服务器都可以缓存。

max-age：缓存内容将在多少秒失效。

no-cache：强制缓存这两个标识无法处理缓存。

no-store：所有内容都不用缓存


---

## 对比缓存

+ 首先需要进行比较判断是否可以使用缓存。

+ 服务器会将缓存标识与数据一起返回给客户端。

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20180302150203.png)

---

ETag/If-None-Match

+ Etag：服务器响应请求时，告诉浏览器当前资源在服务器的唯一标识。

+ If-None-Match：再次请求服务器时，通过此字段通知服务器客户端缓存数据的唯一标识。

---

Last-Modified/If-Modified-Since

+ Last-Modified：服务器在响应请求时，告诉浏览器资源的最后修改时间。

+ If-Modified-Since：再次请求服务器时，通过此字段通知服务器上次请求时，服务器返回的资源最后修改时间。
