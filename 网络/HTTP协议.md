## 概念

> 超文本传输协议(HyperText Transfer Protocol，缩写:HTTP)是互联网上应用最为广泛的一种网络协议。设计HTTP最初的目的是为了提供一种发布和接收HTML页面的方法。通过HTTP或者HTTPS协议请求的资源由统一资源标识符(Uniform Resource Identifiers,URI)来标识。
HTTP构建于TCP/IP协议之上，默认端口是80，HTTP是无连接无状态的。

HTTP协议是基于请求与响应的：<br>
![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/1802251-69b823c79019d0e3.png)

## HTTP报文

1、请求方法

HTTP1.1目前支持7中请求方法。

> 1、GET<br>
GET方法是默认的HTTP请求方法，日常用GET方法来提交表单数据，然而用GET方法提交的表单数据只经过了简单的编码，同时它将作为URL的一部分向WEB服务器发送。因此，如果使用GET方法来提交表单数据就存在安全隐患。
<br>
2、POST<br>
主要向服务器提交数据，尤其是大批量数据。POST方法克服了GET方法的一些缺点。通过POST提交数据时，数据不是作为URL请求的一部分而是作为标准数据传送给服务器，克服了GET中信息无法保密和数据量太小的缺点。
3、HEAD<br>
请求获取有Request-URI所标识的资源的响应消息报头。
<br>
4、OPTIONS<br>
请求查询服务器的性能，或查询与资源相关的选项和需求。
<br>
5、PUT<br>
请求服务器存储一个资源，并用Request-URI作为标识。
<br>
6、DELETE<br>
请求服务器删除由Request-URI标识的资源。
<br>
7、TRACE<br>
请求服务器回送收到的请求信息，主要用于测试或诊断。
<br>

2、请求报文

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/1802251-45ee4c6b408c23c3.png)

请求报文由三个部分组成：

+ 请求行：由请求方法、URL和HTTP协议版本组成，用空格分隔。
+ 请求头：请求头部由关键字/值对组成，关键字和值用英文冒号":"分隔。<br>
![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/1802251-cfd1478b6f8bbd8d.png)
+ 请求正文：请求头与请求正文直接是一个空行，这个行非常重要，它表示请求头已经结束，接下来的是请求正文。请求正文可以包含客户提交的查询字符串信息。

下面是一个典型的请求报文：
<br>
```
GET /sample.jsp HTTP/1.1

Accept:image/gif.image/jpeg,*/*
Accept-Language:zh-cn
Connection:Keep-Alive
Host:localhost
User-Agent:Mozila/4.0(compatible;MSIE5.01;Window NT5.0)
Accept-Encoding:gzip,deflate

username=jinqiao&password=1234
```

3、响应报文

![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/1802251-461f8cd980b99450.png)

响应报文由三部分组成：

+ 状态行：由协议版本、数字形式的状态码以及响应的状态描述，各元素之间以空格分隔。

状态码由三个数字组成，第一个数字定义了响应的类别：<br>
1xx：指示信息--表示请求已接收，继续处理。<br>
2xx：成功--表示请求已被成功接收、理解、接受。<br>
3xx：重定向--要完成请求必须进行更进一步的操作。<br>
4xx：客户端错误--请求有语法错误或请求无法实现。<br>
5xx：服务器端错误--服务器未能实现合法的请求。<br>

常见的状态码如下：<br>
200 OK 客户端请求成功。<br>
301 Moved Permanently 请求永久重定向。<br>
302 Moved Temporarily 请求临时重定向。<br>
304 Not Modified 文件未修改，可以直接使用缓存的文件。<br>
400 Bad Request 由于客户端请求有语法错误，不能被服务器所理解。<br>
401 Unauthorized 请求未经授权。这个状态代码必须和WWW-Authenticate报头域一起使用。<br>
403 Forbidden 服务器收到请求，但是拒绝提供服务。服务器通常会在响应正文中给出不提供服务的原因。<br>
404 Not Found 请求的资源不存在，例如，输入了错误的URL<br>
500 Internal Server Error 服务器发生不可预期的错误，导致无法完成客户端的请求。<br>
503 Service Unavailable 服务器当前不能够处理客户端的请求，在一段时间之后，服务器可能会恢复正常。<br>

+ 响应头：与请求头部类似，为响应报文添加一些附加信息<br>
![](https://gitlab.com/LinHaiGU/Android/raw/master/picture/1802251-77792a3b75b72804.png)

+ 响应正文：用于存放需要返回给客户端的数据信息。


一个典型的响应报文如下：<br>
```
HTTP/1.1 200 OK　　状态行
Date: Sun, 17 Mar 2013 08:12:54 GMT　　响应头部
Server: Apache/2.2.8 (Win32) PHP/5.2.5
X-Powered-By: PHP/5.2.5
Set-Cookie: PHPSESSID=c0huq7pdkmm5gg6osoe3mgjmm3; path=/
Expires: Thu, 19 Nov 1981 08:52:00 GMT
Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0
Pragma: no-cache
Content-Length: 4393
Keep-Alive: timeout=5, max=100
Connection: Keep-Alive
Content-Type: text/html; charset=utf-8
　　空行

<html>　　响应数据
<head>
<title>HTTP响应示例<title>
</head>
<body>
Hello HTTP!
</body>
</html>
```

## 无连接和无状态

1、无连接

> 它的含义是限制每次连接只处理一个请求。服务器处理完客户的请求，并收到客户的应答后，即断开连接。采用这种方式可以节省传输时间。

然而随着互联网的发展，一台服务器同一时间处理的请求越来越多，如果依然采用原来的方式，将会在建立和断开连接上花费大部分时间。

HTTP/1.0：持久连接被提出来；即当一个TCP连接服务器多次请求：客户端会在请求Header中携带Connection:Keep-Alive;向服务器请求持久连接，如果服务端允许就会在响应报文中加上相同的字段。

HTTP/1.1时代：持久连接称为了默认的连接方式；同时持久连接的弊病也展现出来，即所有的连接都是串行的，HOLB；当某一个请求阻塞时就会导致同一条连接的后续请求被阻塞；

为了解决这一问题：提出了流水线技术(pipellining)的概念；客户端发起一次请求时不必等待响应便直接发起第二个请求；服务端按照请求的顺序一次返回结果。

我们知道 HTTP 协议采用“请求-应答”模式，当使用普通模式，即非 Keep-Alive 模式时，每个请求/应答客户和服务器都要新建一个连接，完成之后立即断开连接（HTTP协议为无连接的协议）；当使用 Keep-Alive 模式（又称持久连接、连接重用）时，Keep-Alive 功能使客户端到服务器端的连接持续有效，当出现对服务器的后继请求时，Keep-Alive 功能避免了建立或者重新建立连接。

+ HTTP Keep-Alive 简单说就是保持当前的TCP连接，避免了重新建立连接。
+ HTTP 长连接不可能一直保持，例如 Keep-Alive: timeout=5, max=100，表示这个TCP通道可以保持5秒，max=100，表示这个长连接最多接收100次请求就断开。
+ HTTP是一个无状态协议，这意味着每个请求都是独立的，Keep-Alive没能改变这个结果。另外，Keep-Alive也不能保证客户端和服务器之间的连接一定是活跃的，在HTTP1.1版本中也如此。唯一能保证的就是当连接被关闭时你能得到一个通知，所以不应该让程序依赖于Keep-Alive的保持连接特性，否则会有意想不到的后果。
+ 使用长连接之后，客户端、服务端怎么知道本次传输结束呢？两部分：1. 判断传输数据是否达到了Content-Length 指示的大小；2. 动态生成的文件没有 Content-Length ，它是分块传输（chunked），这时候就要根据 chunked 编码来判断，chunked 编码的数据在最后有一个空 chunked 块，表明本次传输数据结束。

2、无状态

> 无状态是指协议对于事务处理没有记忆能力，服务器不知道客户端是什么状态。即我们给服务器发送 HTTP 请求之后，服务器根据请求，会给我们发送数据过来，但是，发送完，不会记录任何信息。HTTP 是一个无状态协议，这意味着每个请求都是独立的，Keep-Alive 没能改变这个结果。















